/*
* Copyright (c) 2024 Huawei Device Co., Ltd. All rights reserved
* Use of this source code is governed by a MIT license that can be
* found in the LICENSE file
*/

#include "generated/RNOH/generated/BaseReactNativeBackgroundTimerPackage.h"
#pragma once

namespace rnoh {
class BackgroundTimerPackage : public BaseReactNativeBackgroundTimerPackage {
    using Super = BaseReactNativeBackgroundTimerPackage;
    using Super::Super;
};
} // namespace rnoh